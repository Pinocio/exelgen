<?php 
class ModelExtensionModuleExelGen extends Model
{
	public function getDefaultFileList()
	{
		$sql = "SELECT product_id, model, image, product_type FROM " . DB_PREFIX . "product";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function updateUrlFile($id, $url)
	{
		$sql= "UPDATE ". DB_PREFIX ."product SET image='".$this->db->escape($url)."' WHERE product_id='".(int)$id."'";
		$this->db->query($sql);
		$this->cache->delete('product');
	}
	public function getAllCategoriesList()
	{
		$query = $this->db->query("SELECT cc.category_id, cc.parent_id, cc.status, cd.name, cp.path_id as type_id, cd2.name as type FROM ".DB_PREFIX."category cc LEFT JOIN ".DB_PREFIX."category_description cd ON(cc.category_id = cd.category_id) LEFT JOIN ".DB_PREFIX."category_path cp ON(cc.category_id = cp.category_id AND cp.level = 0) LEFT JOIN ".DB_PREFIX."category_description cd2 ON(cp.path_id = cd2.category_id)");
		return $query->rows;
	}
}