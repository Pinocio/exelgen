<?php
class ControllerExtensionModuleExelGen extends Controller {
	
	private $error = array();

	public function index() {
		//1532
		$this->load->model('catalog/product');
		$this->load->language('extension/module/exelgen');
		if (method_exists($this->document, 'setTitle')) {
			$this->document->setTitle($this->language->get('title'));
		} else {
			$this->document->title = $this->language->get('title'); //load title
		}

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('exelgen', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
		}

		$data['heading_title']       = $this->language->get('heading_title');

		$data['text_edit']           = $this->language->get('text_edit');
		$data['text_enabled']        = $this->language->get('text_enabled');
		$data['text_disabled']       = $this->language->get('text_disabled');

		$data['button_save']         = $this->language->get('button_save');
		$data['button_cancel']       = $this->language->get('button_cancel');
		$data['button_generate']       = $this->language->get('text_btn_generate');
		$data['button_download']       = $this->language->get('text_btn_download');
		

		$data['entry_name']          = $this->language->get('entry_name');
		$data['entry_heading_title'] = $this->language->get('entry_heading_title');
		$data['entry_text_main']     = $this->language->get('entry_text_main');
		$data['entry_email_status']  = $this->language->get('entry_email_status');
		$data['entry_header_status'] = $this->language->get('entry_header_status');
		$data['entry_status']        = $this->language->get('entry_status');
		$data['text_title_files_list'] = $this->language->get('text_title_files_list');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/exelgen', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('extension/module/exelgen', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);

		if (isset($this->request->post['exelgen_status'])) {
			$data['exelgen_status'] = $this->request->post['exelgen_status'];
		} else {
			$data['exelgen_status'] = $this->config->get('exelgen_status');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['token'] = $this->session->data['token'];
		$data['files'] = $this->getListFiles();
		$this->response->setOutput($this->load->view('extension/module/exelgen.tpl', $data));
	}

	public function exelgen()
	{	
		$start = microtime(true);
		$this->load->language('extension/module/exelgen');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');

		if (isset($this->request->post['type_sort'])) {
			$type_sort = $this->request->post['type_sort'];
		} else {
			$type_sort = 0;
		}
		if (isset($this->request->post['filter_active_product'])){
			$filter_active_product = $this->request->post['filter_active_product'];
		} else {
			$filter_active_product = true;
		}
		if (isset($this->request->post['filter_active_category'])){
			$filter_active_category = $this->request->post['filter_active_category'];
		} else {
			$filter_active_category = true;
		}

		$product_count = 0; //калькулятор иттераций

		$data = array();
		$json = array();
		$categories_info = array();
		$predata_products = array();
		$success_products = array();

		$this->load->model('tool/image');
		$this->load->model('extension/module/exelgen');

		$categories = $this->model_extension_module_exelgen->getAllCategoriesList();
		// $json['categories'] = $categories;
		foreach ($categories as $category)
		{	
			if($filter_active_category === 'true'){
				if ($category['status'] != 1) continue;
			}
			$products = $this->model_catalog_product->getProductsByCategoryId($category['category_id']);


			/* oleg ti dolbaEB */
			$categories_info[$category['category_id']] = array(
				'id' => $category['category_id'],
				'name' => $category['name']
			);

			foreach($products as $product)
			{	
				if($filter_active_product === 'true'){
					if($product['status'] != 1) continue;
				}

				$categories_ids = $this->model_catalog_product->getProductCategories($product['product_id']);
				if(!empty($category['type_id'])){
					$type_id = $category['type_id'];
				}else{
					$type_id = '';
				}
				$predata_products[$product['product_id']] = array(
					'articul' => $product['model'],
					'type' => str_replace("&quot;", "",$category['type']),
					'type_id' => $type_id,
					'name' => $product['name'],
					'price' => $product['price'],
					'status' => $product['status'],
					'image' => $product['image']
				);
				$json['imageUrl'][] = $this->model_tool_image->resize($product['image'], 1600, 1600);
				foreach($categories_ids as $category_id)
				{	
					if(array_key_exists($category_id, $categories_info))
					{
						$predata_products[$product['product_id']]['categories'][] = $categories_info[$category_id];
					}
				}
				$product_count += 1;
			}
		}
		$json['predata_products'] = $predata_products;
		require_once(DIR_SYSTEM . 'PHPExcel/Classes/PHPExcel.php');
		$xls = new PHPExcel();

		if ($type_sort == 'of_count')
		{	
			$success_products = array_chunk($predata_products, 1000);
			$index = 0;
			foreach ($success_products as $key => $products)
			{
				$title_1 = $index * 1000;
				if ($title_1 == 0) $title_1 = 1;
				$title_2 = $title_1 + 999;
				if ($title_2 > count($predata_products)) $title_2 = count($predata_products);
				$title = $title_1 . ' - ' . $title_2;
				$this->createDataTables($xls, $title, $products, $index);
				$index++;
			}
		}

		if ($type_sort == 'of_types')
		{	
			$index = 0;
			ksort($predata_products);
			foreach ($predata_products as $product)
			{	
				$success_products[$product['type_id']][] = $product;
			}
			// $json['success_products'] = $success_products;
			foreach ($success_products as $key => $products)
			{	
				if ($key != ''){
					$key_type = 'type_'.$key;
					$title = $this->language->get('text_'.$key_type);
					$this->createDataTables($xls, $title, $products, $index);
					$index++;
				}
			}
		}
		if ($type_sort == 'of_all')
		{	
			$index = 0;
			$title = '0 - ' . count($predata_products);
			$this->createDataTables($xls, $title, $predata_products, $index);
		}

		$objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel5');
		$date = date('YmdHis');
		$link = 'exelgen/'.$date.'_pricelist_'.$type_sort.'.xls';
		$objWriter->save('../'.$link);

		$finish = microtime(true);
		$json['delta'] = ($finish - $start) . ' секунд'; //потраченое время
		$json['product_count'] = $product_count; //калькулятор итераций
		$json['elements'] = count($predata_products); //колличество элементов
		$json['document_link'] = HTTPS_CATALOG.$link;
		
		$this->response->setOutput(json_encode($json));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/exelgen')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}

	protected function getListFiles()
	{
		$path = $this->request->server['DOCUMENT_ROOT'].'/exelgen/';
		if (!is_dir($path)) mkdir($path,0755,true);
		$files = scandir($path);
		unset($files[0],$files[1]);
		return $files;
	}
	public function delete(){
		$json = array();
		if (isset($this->request->post['file'])) {
			unlink($this->request->server['DOCUMENT_ROOT'].'/exelgen/'.$this->request->post['file']);
		}
		$this->response->setOutput(json_encode($json));
	}
	public function createDataTables($xls, $title = 'title', $products = array(), $index = 0)
	{	
		$this->load->language('extension/module/exelgen');
		$this->load->model('tool/image');
		
		$y = 1; //row counter

		$page = $xls->createSheet($index);
		$page = $xls->setActiveSheetIndex($index);

		$page->setTitle($title);

		$page->setCellValue('A1',  $this->language->get('entry_col_articul')); //0
		$page->setCellValue('B1',  $this->language->get('entry_col_type')); //1
		$page->setCellValue('C1',  $this->language->get('entry_col_name')); //2
		$page->setCellValue('D1',  $this->language->get('entry_col_category')); //3
		$page->setCellValue('E1',  $this->language->get('entry_col_price')); //4
		$page->setCellValue('F1',  $this->language->get('entry_col_image')); //5

		$page->getColumnDimension("A")->setAutoSize(true);
		$page->getColumnDimension("B")->setAutoSize(true);
		$page->getColumnDimension("C")->setAutoSize(true);
		$page->getColumnDimension('D')->setAutoSize(true);
		$page->getColumnDimension("E")->setAutoSize(true);
		$page->getColumnDimension('F')->setWidth(23);

		foreach ($products as $product)
		{	
			$y++;
			$imagePath = str_replace(HTTPS_CATALOG, $this->request->server['DOCUMENT_ROOT'].'/', $this->model_tool_image->resize($product['image'], 150, 150));
			$imageUrl = $this->model_tool_image->resize($product['image'], 1600, 1600);
			$page->setCellValueByColumnAndRow(0, $y, $product['articul']); //draw articul
			$page->setCellValueByColumnAndRow(1, $y,  $product['type']); //draw type
			$page->setCellValueByColumnAndRow(2, $y, html_entity_decode($product['name'])); //draw name
			$page->getCell('C'.$y)->getHyperlink()->setUrl($imageUrl);

			$path_catalog = '';
			foreach($product['categories'] as $key => $value){
				$cat_line = html_entity_decode($value['name']);
				// if(stripos($cat_line, '>')){
				// 	$cat_string = explode('>',$cat_line, 2)[1];
				// 	$path_catalog = '|||'.$path_catalog.$cat_string.chr(10).chr(13).'|'.PHP_EOL;
				// }else{
				// 	$path_catalog .= $cat_line;
				// }
				if ($key != 0){
					$path_catalog .= chr(10);
				}
				$path_catalog .= $cat_line;
			}

			$page->setCellValueByColumnAndRow(3, $y, $path_catalog); //draw categories
			$page->setCellValueByColumnAndRow(4, $y, $product['price']); //draw price

			if (file_exists($imagePath)) {
				$image = new PHPExcel_Worksheet_Drawing();
				$image->setPath($imagePath);
				$image->setCoordinates('F'.$y);
				$image->setOffsetX(4);
				$image->setOffsetY(2);
				$page->getRowDimension($y)->setRowHeight(150);
				$image->setWorksheet($page);
			} else {
				$page->setCellValueByColumnAndRow(5, $y, 'Ошибка записи картинки. Файл не существует.');
			}
			$styleArray = array('font' => array('color'=>array('rgb'=>'4573b1')));
			$page->getStyle('A'.$y)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$page->getStyle('A'.$y)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$page->getStyle('B'.$y)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$page->getStyle('B'.$y)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$page->getStyle('C'.$y)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$page->getStyle('C'.$y)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$page->getStyle('C'.$y)->applyFromArray($styleArray);
			$page->getStyle('D'.$y)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$page->getStyle('D'.$y)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$page->getStyle('E'.$y)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$page->getStyle('E'.$y)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$page->getStyle('F'.$y)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$page->getStyle('F'.$y)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$page->getStyle('G'.$y)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$page->getStyle('G'.$y)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$page->getStyle('D'.$y)->getAlignment()->setWrapText(true);
		}
	}
	/*
	public function renameFiles(){
		$this->load->model('extension/module/exelgen');
		$root_path = $this->request->server['DOCUMENT_ROOT'] . "/image/";
		if (!file_exists($root_path.'catalog/products/')){
			mkdir($root_path.'catalog/products/');
		}
		$json = array();
		$data = array();
		$file_list = $this->model_extension_module_exelgen->getDefaultFileList();
		foreach ($file_list as $item)
		{
			$model = $item['model'];
			$type_image = end(explode('.',$item['image']));
			$path = explode('/',$item['image'])[0];
			if (!empty($item['product_type'])){
				$type_product = $item['product_type'];
			} else {
				$type_product = 'non_type';
			}
			$data[] = array(
				'id' => $item['product_id'],
				'model' => $this->translit($model),
				'type_image' => $type_image,
				'path' => $path,
				'file' => $root_path.$item['image'],
				'type_product' => $type_product
			);
		}
		foreach($data as $item)
		{
			$dir = 'catalog/products/'.$item['type_product'].'/'.$item['id'].'/';
			$file_path = $dir.$item['model'].'.'.$item['type_image'];
			if (!file_exists($root_path.$dir)){
				mkdir($root_path.$dir, 0755, true);
			}
			if(file_exists($item['file'])){
				if(!copy($item['file'],$root_path.$file_path)){
					$json['errors']['copy'][$item['id']]['files'] = $item['file'];
					$json['errors']['copy'][$item['id']]['path'] = $root_path.$file_path;
					$json['errors']['copy'][$item['id']]['error'] = 'Не получилось копировать';
				}else{
					$this->model_module_exelgen->updateUrlFile($item['id'], $file_path);
				}
			}else{
				$json['errors']['files'][] = $item['file'];
			}
		}
		$this->response->setOutput(json_encode($json));
	}
	*/
	private function translit($s) {
		$s = (string) $s; // преобразуем в строковое значение
		$s = strip_tags($s); // убираем HTML-теги
		$s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
		$s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
		$s = trim($s); // убираем пробелы в начале и конце строки
		$s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
		$s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
		$s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
		$s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
		return $s; // возвращаем результат
	  }
}